package pducom

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

func testDecode(t *testing.T, in string, expected interface{}) {
	s, err := nmea.ParseSentence([]byte(in))
	if err != nil {
		t.Fatal(err)
	}
	obj, err := DecodeSentence(RefmtSentence(s))
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(obj, expected) {
		t.Errorf("Decode failed; expected %#v, got %#v", expected, obj)
	}
}

func TestDecodeSentence(t *testing.T) {
	tests := map[string]struct {
		in  string
		out interface{}
	}{
		"adc-0": {in: "$ADC0R,0000,0004,0005,0004,0007,0004,0004,54E8*5A",
			out: RawAdc{Data: [8]uint16{0, 4, 5, 4, 7, 4, 4, 0x54e8}}},
		"adc-1": {in: "$ADC1R,006C,E6FE,D33C,0FED,658E,1114,0008,002D*55",
			out: RawAdc{Data: [8]uint16{0x6c, 0xe6fe, 0xd33c, 0x0fed, 0x658e, 0x1114, 8, 0x2d}}},
		"hcb.adc-1": {in: "$HCBRX,ADC,0060,0211,1234,5678,0000,1114,0008,0020",
			out: HcbAdc{Data: [8]uint16{60, 211, 1234, 5678, 0, 1114, 8, 20}}},
		"hcb.adc-2": {in: "$HCB,ADC,0060,0211,1234,5678,0000,1114,0008,0020",
			out: HcbAdc{Data: [8]uint16{60, 211, 1234, 5678, 0, 1114, 8, 20}}},
		"hcb.adc-3": {in: "$HCB,ADC,0046,0000,0000,0000,0660,0661,0663,0661*22",
			out: HcbAdc{Data: [8]uint16{46, 0, 0, 0, 660, 661, 663, 661}}},
		"bmepr": {in: "$BMEPR,2774,1574,100389*66",
			out: ScaledEnv{Temp: 27.74, Rh: 15.74, Pr: 1.00389}},
		"imagr": {in: "$IMAGR,-204,7,16093,-139,-108,-86,280*76",
			out: Imu{Accel: [3]int32{-204, 7, 16093},
				Gyro: [3]int32{-139, -108, -86}, Temp: 280}},
		"imags": {in: "$IMAGS,-13,0,981,-1217,-1015,-727,2800",
			out: ScaledImu{Accel: [3]float32{-0.013, 0, 0.981},
				Gyro: [3]float32{-1.217, -1.015, -0.727}, Temp: 28}},
		"power": {in: "$POWER,TB,0,UV,1,BEXP,0,",
			out: Power{"TB": false, "UV": true, "BEXP": false}},
		"hcb.alltemp-0": {in: "$HCBRX,ALLTEMP,0000000234,0019,044,22,255",
			out: HcbTemp{Time: 234, Heatsink: 19, Heatpad: 44, Avg: 22, Pwm: 255}},
		"hcb.alltemp-1": {in: "$HCBRX,ALL_TEMP,0000000234,19,44,22",
			out: HcbTemp{Time: 234, Heatsink: 19, Heatpad: 44, Avg: 22, Pwm: 0}},
		"hcb.alltemp-2": {in: "$HCB,ALL_TEMP,0000000234,19,44,22",
			out: HcbTemp{Time: 234, Heatsink: 19, Heatpad: 44, Avg: 22, Pwm: 0}},
		"hcb.heatpad": {in: "$HCB,HEATPAD_PWM,2048",
			out: HcbSetting{Key: "HEATPAD_PWM", Value: 2048}},
		"hcb.heatpad-set-1": {in: "$HCB,SET,HEATPAD_PWM,42",
			out: HcbSetting{Key: "HEATPAD_PWM", Value: 42}},
		"hcb.heatpad-set-2": {in: "$HCBRX,SET,HEATPAD_PWM,43",
			out: HcbSetting{Key: "HEATPAD_PWM", Value: 43}},
		"hcb.fan-1": {in: "$HCB,FAN1,16",
			out: HcbSetting{Key: "FAN1", Value: 16}},
		"hcb.fan-2": {in: "$HCB,FAN2,16",
			out: HcbSetting{Key: "FAN2", Value: 16}},
		"hcb.time": {in: "$HCB,TIME,0000000234",
			out: HcbSetting{Key: "TIME", Value: 234}},
		"bab-1": {in: "$BAB,BAT,20,16812,00000,089",
			out: Battery{Count: 20, Voltage: 16812, Current: 0, Capacity: 89}},
		"bab-2": {in: "$BAB,BAT,20,16812,-00042,089",
			out: Battery{Count: 20, Voltage: 16812, Current: -42, Capacity: 89}},
		"bab-3": {in: "$BAB,BAT,20,16812,-42,089",
			out: Battery{Count: 20, Voltage: 16812, Current: -42, Capacity: 89}},
		"status": {in: "$STATS,74,2985,0,1*45",
			out: Status{Boot: 74, Uptime: 2985, ChgFault: 0, ChgStatus: 1}},
		"bstat": {in: "$BSTAT,13,0*62",
			out: Bstat{Boot: 13, Uptime: 0}},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			testDecode(t, tc.in, tc.out)
		})
	}
}

func TestBadInput(t *testing.T) {
	in := "$ADC0R,0000,0004,0005,0004,0007,0004"
	s := nmea.Sentence{}
	err := s.UnmarshalText([]byte(in))
	if err != nil {
		t.Fatal(err)
	}
	adc := RawAdc{}
	err = UnmarshalNMEA(s, &adc)
	if err == nil {
		t.Error("Bad input not detected")
	} else {
		t.Log(err)
	}
}

func TestPwren(t *testing.T) {
	table := []struct {
		states []PwrState
		output string
	}{
		{
			states: []PwrState{{Sys: TimingBoard, Enabled: true},
				{Sys: Camera1}, {Sys: Camera2, Enabled: true}},
			output: "$PWREN,TB,1,CAM1,0,CAM2,1",
		},
		{
			states: []PwrState{{Sys: "invalid", Enabled: true},
				{Sys: Camera1}, {Sys: Camera2, Enabled: true}},
			output: "$PWREN,CAM1,0,CAM2,1",
		},
	}

	for _, e := range table {
		s := Pwren(e.states...)
		text := s.String()
		if !strings.HasPrefix(text, e.output) {
			t.Errorf("Bad sentence; expected %q, got %q", e.output, text)
		}
	}
}

type stringRW struct {
	*strings.Reader
	*strings.Builder
}

func newRW(text string) stringRW {
	rw := stringRW{}
	rw.Builder = &strings.Builder{}
	rw.Reader = strings.NewReader(text)
	return rw
}

func ExampleScan() {
	input := `$ADC1R,006C,E6FE,D33C,0FED,658E,1114,0008,002D*55
$ADC0R,0000,0004,0005,0004,0007,0004,0004,54E8*5A
$ADC4R,0000,0004,0005,0004,0007,0004,0004,54E8*5E
$IMAGR,-204,7,16093,-139,-108,-86,280
$IMAGS,-13,0,981,-1217,-1015,-727,2800
$FDBUG,0,0,0,0,0,0AB
$FSTAT,LIMIT,,2342
$FSTAT,SEEK,BRAKE,2341
$BMEPR,2774,1574,100389*66
$FOCUS,JOG,F,10000
`
	rw := newRW(input)
	dev := NewDevice(rw)
	for dev.Scan() {
		s := dev.Sentence()
		if v, _ := DecodeSentence(s); v != nil {
			fmt.Printf("%s\n", v)
		} else {
			fmt.Printf("%s\n", s)
		}
	}
	// Output:
	// [6c e6fe d33c fed 658e 1114 8 2d]
	// [0 4 5 4 7 4 4 54e8]
	// [0 4 5 4 7 4 4 54e8]
	// [-204 7 16093],[-139 -108 -86],280
	// [-0.013 0.000 0.981],[-1.217 -1.015 -0.727],28.00
	// 0,0,0,0,0,171
	// LIMIT,false,2342
	// SEEK,true,2341
	// 27.74,15.7,1.00389
	// JOG,[F 10000]
}

func ExampleScanWithError() {
	input := `!! diagnostic message
06C,E6FE,D33C,0FED,658E,1114,0008,002D*55
$ADC0R,0000,0004,0005,0004,0007,0004,0004,54E8*5A
$IMAGR,-204,7,16093,-139,-108,-86,280*76
$IMAGS,-13,0,981,-1217,-1015,-727,2800
$BMEPR,2774,1574,100389*66
`
	rw := newRW(input)
	dev := NewDevice(rw)
	for dev.Scan() {
		s := dev.Sentence()
		if v, _ := DecodeSentence(s); v != nil {
			fmt.Printf("%s\n", v)
		}
	}
	// Output:
	// [0 4 5 4 7 4 4 54e8]
	// [-204 7 16093],[-139 -108 -86],280
	// [-0.013 0.000 0.981],[-1.217 -1.015 -0.727],28.00
	// 27.74,15.7,1.00389
}

func ExampleJSONOutput() {
	input := `$ADC1R,006C,E6FE,D33C,0FED,658E,1114,0008,002D*55
$ADC0R,0000,0004,0005,0004,0007,0004,0004,54E8*5A
$IMAGR,-204,7,16093,-139,-108,-86,280*76
$IMAGS,-13,0,981,-1217,-1015,-727,2800
$BMEPR,2774,1574,100389*66
$POWER,CCD,1,BEXP,1,UV,0
`
	tfake := time.Date(2006, time.January, 2, 15, 4, 5, 0, time.UTC)
	rw := newRW(input)
	dev := NewDevice(rw)
	for dev.Scan() {
		s := dev.Sentence()
		data, _ := DecodeSentence(s)
		msg := struct {
			T    time.Time   `json:"time"`
			Src  string      `json:"source"`
			Data interface{} `json:"data"`
		}{T: tfake, Src: s.Id, Data: data}
		b, _ := json.Marshal(msg)
		fmt.Println(string(b))
	}
	// Output:
	// {"time":"2006-01-02T15:04:05Z","source":"ADC1R","data":{"scan":[108,59134,54076,4077,25998,4372,8,45]}}
	// {"time":"2006-01-02T15:04:05Z","source":"ADC0R","data":{"scan":[0,4,5,4,7,4,4,21736]}}
	// {"time":"2006-01-02T15:04:05Z","source":"IMAGR","data":{"accel":[-204,7,16093],"gyro":[-139,-108,-86],"temp":280}}
	// {"time":"2006-01-02T15:04:05Z","source":"IMAGS","data":{"accel":[-0.013,0,0.981],"gyro":[-1.217,-1.015,-0.727],"temp":28}}
	// {"time":"2006-01-02T15:04:05Z","source":"BMEPR","data":{"temp":27.74,"rh":15.74,"pr":1.00389}}
	// {"time":"2006-01-02T15:04:05Z","source":"POWER","data":{"BEXP":true,"CCD":true,"UV":false}}
}

func BenchmarkDecodeLongSentence(b *testing.B) {
	s, _ := nmea.ParseSentence([]byte("$POWER,TB,0,UV,0,BEXP,0,CCD,0,HCB,1,HCBMODE,0,CAM1,0,CAM2,0,LED1,0,LED2,0,EXPAN,0,FOCUS,0,LASER5,0,LASER12,0,LASER28,0*48"))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		DecodeSentence(s)
	}
}

func BenchmarkDecodeShortSentence(b *testing.B) {
	s, _ := nmea.ParseSentence([]byte("$FDBUG,520736,520735,520734,520735,0,800*58"))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		DecodeSentence(s)
	}
}
