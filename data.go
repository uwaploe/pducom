package pducom

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

// NMEAUnmarshaler is the interface implemented by an object that can unmarshal
// itself from an NMEA sentence.
type NMEAUnmarshaler interface {
	UnmarshalNMEA(s nmea.Sentence) error
}

// $ADCxR sentence
type RawAdc struct {
	Data [8]uint16 `json:"scan" hex:""`
}

type HcbAdc struct {
	Data [8]uint16 `json:"scan"`
}

// String implements the Stringer interface
func (a RawAdc) String() string {
	return fmt.Sprintf("%x", a.Data)
}

// $IMAGR/$IMAGS sentence
type Imu struct {
	Accel [3]int32 `json:"accel"`
	Gyro  [3]int32 `json:"gyro"`
	Temp  int32    `json:"temp"`
}

// String implements the Stringer interface
func (imu Imu) String() string {
	return fmt.Sprintf("%d,%d,%d",
		imu.Accel,
		imu.Gyro,
		imu.Temp)
}

// Scale creates a ScaleImu value from an Imu value
func (imu Imu) Scale() ScaledImu {
	var simu ScaledImu
	for i := 0; i < 3; i++ {
		simu.Accel[i] = float32(imu.Accel[i]) / AccelScale
		simu.Gyro[i] = float32(imu.Gyro[i]) / GyroScale
	}
	simu.Temp = float32(imu.Temp) / TempScale
	return simu
}

// String implements the Stringer interface
func (imu ScaledImu) String() string {
	return fmt.Sprintf("%.3f,%.3f,%.2f",
		imu.Accel,
		imu.Gyro,
		imu.Temp)
}

// Multipliers for scaled data values.
const (
	AccelScale   float32 = 1000
	GyroScale            = 1000
	TempScale            = 100
	MagScale             = 1000
	EnvTempScale         = 100
	RhScale              = 100
	PrScale              = 1e5
)

// Converted $IMAGS sentence
type ScaledImu struct {
	// Acceleration in G
	Accel [3]float32 `json:"accel"`
	// Angular rates in deg/s
	Gyro [3]float32 `json:"gyro"`
	// Temperature in degC
	Temp float32 `json:"temp"`
}

// $BMEPR sentence
type Env struct {
	// Temperature in degC * 100
	Temp int32 `json:"temp"`
	// Relative humidty in % * 100
	Rh int32 `json:"rh"`
	// Pressure in Pa
	Pr uint32 `json:"pr"`
}

// Scale creates a ScaleEnv value from an Env value
func (e Env) Scale() ScaledEnv {
	var senv ScaledEnv
	senv.Temp = float32(e.Temp) / EnvTempScale
	senv.Rh = float32(e.Rh) / RhScale
	senv.Pr = float32(e.Pr) / PrScale
	return senv
}

// String implements the Stringer interface
func (e Env) String() string {
	return fmt.Sprintf("%d,%d,%d",
		e.Temp,
		e.Rh,
		e.Pr)
}

// Scaled version of the Env struct
type ScaledEnv struct {
	// Temperature in degC
	Temp float32 `json:"temp"`
	// Relative humidity in %
	Rh float32 `json:"rh"`
	// Pressure in bars
	Pr float32 `json:"pr"`
}

// String implements the Stringer interface
func (e ScaledEnv) String() string {
	return fmt.Sprintf("%.2f,%.1f,%.5f",
		e.Temp,
		e.Rh,
		e.Pr)
}

// $IMMGR/$IMMGS sentence
type Mag struct {
	X int32 `json:"x"`
	Y int32 `json:"y"`
	Z int32 `json:"z"`
}

// String implements the Stringer interface
func (m Mag) String() string {
	return fmt.Sprintf("%d,%d,%d",
		m.X,
		m.Y,
		m.Z)
}

// Scale creates a ScaledMag value from an Mag value
func (m Mag) Scale() ScaledMag {
	var smag ScaledMag
	smag.X = float32(m.X) / MagScale
	smag.Y = float32(m.Y) / MagScale
	smag.Z = float32(m.Z) / MagScale
	return smag
}

// Scaled version of the Mag struct
type ScaledMag struct {
	// X magnetometer component in ??
	X float32 `json:"x"`
	// Y magnetometer component in ??
	Y float32 `json:"y"`
	// Z magnetometer component in ??
	Z float32 `json:"z"`
}

// String implements the Stringer interface
func (m ScaledMag) String() string {
	return fmt.Sprintf("%.2f,%.2f,%.2f",
		m.X,
		m.Y,
		m.Z)
}

// $POWER/$PWREN sentence
type Power map[string]bool

func (p Power) String() string {
	return fmt.Sprintf("%#v", p)
}

func (p *Power) UnmarshalNMEA(s nmea.Sentence) error {
	vals := make(map[string]bool)
	n := len(s.Fields)
	for i := 0; i < n; i += 2 {
		key := s.Fields[i]
		// Work-around for the (invalid) trailing comma in the $POWER
		// NMEA sentence which results in an empty field.
		if key == "" {
			break
		}
		if (i + 1) < n {
			vals[key] = s.Fields[i+1] == "1"
		}
	}
	*p = vals
	return nil
}

// $FDBUG sentence
type FocusDebug struct {
	// Encoder A falling edges
	EncAFalling uint32 `json:"enc_a_falling"`
	// Encoder A rising edges
	EncARising uint32 `json:"enc_a_rising"`
	// Encoder B falling edges
	EncBFalling uint32 `json:"enc_b_falling"`
	// Encoder B rising edges
	EncBRising uint32 `json:"enc_b_rising"`
	// Encoder error count
	Nerrors uint32 `json:"errors"`
	// DAC setting
	Dac uint32 `json:"dac" hex:""`
}

type uintParser struct {
	err error
}

func (p *uintParser) Parse(s string, base int) uint32 {
	if p.err != nil {
		return 0
	}
	var val uint64
	val, p.err = strconv.ParseUint(s, base, 32)

	return uint32(val)
}

func (f FocusDebug) String() string {
	return fmt.Sprintf("%d,%d,%d,%d,%d,%d",
		f.EncAFalling,
		f.EncARising,
		f.EncBFalling,
		f.EncBRising,
		f.Nerrors,
		f.Dac)
}

func (f *FocusDebug) UnmarshalNMEA(s nmea.Sentence) error {
	if len(s.Fields) == 0 {
		return fmt.Errorf("Invalid $FDBUG sentence: %q", s)
	}
	p := &uintParser{}
	f.EncAFalling = p.Parse(string(s.Fields[0]), 10)
	f.EncARising = p.Parse(string(s.Fields[1]), 10)
	f.EncBFalling = p.Parse(string(s.Fields[2]), 10)
	f.EncBRising = p.Parse(string(s.Fields[3]), 10)
	f.Nerrors = p.Parse(string(s.Fields[4]), 10)
	f.Dac = p.Parse(string(s.Fields[5]), 16)

	return p.err
}

// $FSTAT sentence
type FocusStatus struct {
	State   string `json:"state"`
	Braking bool   `json:"braking"`
	Counts  int32  `json:"counts"`
}

func (f FocusStatus) String() string {
	return fmt.Sprintf("%s,%t,%d", f.State, f.Braking, f.Counts)
}

func (f *FocusStatus) UnmarshalNMEA(s nmea.Sentence) error {
	if len(s.Fields) == 0 {
		return fmt.Errorf("Invalid $FSTAT sentence: %q", s)
	}
	f.State = s.Fields[0]
	f.Braking = s.Fields[1] == "BRAKE"
	if x, err := strconv.Atoi(s.Fields[2]); err != nil {
		return fmt.Errorf("Invalid COUNTS value: %q", s.Fields[2])
	} else {
		f.Counts = int32(x)
	}

	return nil
}

// $FOCUS sentence. This is actually a command but is echoed back by
// the PDU on success so we need to log it.
type FocusCmd struct {
	Cmd  string   `json:"cmd"`
	Args []string `json:"args"`
}

func (f FocusCmd) String() string {
	return fmt.Sprintf("%s,%s", f.Cmd, f.Args)
}

func (f *FocusCmd) UnmarshalNMEA(s nmea.Sentence) error {
	if len(s.Fields) == 0 {
		return fmt.Errorf("Invalid $FOCUS sentence: %q", s)
	}
	f.Cmd = s.Fields[0]
	f.Args = make([]string, 0, len(s.Fields)-1)
	for _, arg := range s.Fields[1:] {
		f.Args = append(f.Args, arg)
	}

	return nil
}

type intParser struct {
	err error
}

func (p *intParser) Parse(s string) int {
	if p.err != nil {
		return 0
	}
	var val int
	val, p.err = strconv.Atoi(s)

	return val
}

//$HCB,ALLTEMP
type HcbTemp struct {
	Time     int `json:"time"`
	Heatsink int `json:"t_heatsink"`
	Heatpad  int `json:"t_heatpad"`
	Avg      int `json:"t_avg"`
	Pwm      int `json:"pwm"`
}

func (h HcbTemp) String() string {
	return fmt.Sprintf("%d,%d,%d,%d,%d",
		h.Time,
		h.Heatsink,
		h.Heatpad,
		h.Avg,
		h.Pwm)
}

func (h *HcbTemp) UnmarshalNMEA(s nmea.Sentence) error {
	n := len(s.Fields)
	if n < 4 {
		return fmt.Errorf("Invalid $HCB,ALLTEMP sentence: %q", s)
	}

	p := &intParser{}
	h.Time = p.Parse(s.Fields[0])
	h.Heatsink = p.Parse(s.Fields[1])
	h.Heatpad = p.Parse(s.Fields[2])
	h.Avg = p.Parse(s.Fields[3])
	if n > 4 {
		h.Pwm = p.Parse(s.Fields[4])
	}

	return p.err
}

// $HCB,setting,value
type HcbSetting struct {
	Key   string `json:"key"`
	Value int32  `json:"value"`
}

func (h HcbSetting) String() string {
	return fmt.Sprintf("%s,%d", h.Key, h.Value)
}

// $BAB sentence
// $BAB,BAT,20,16812,00000,089
type Battery struct {
	// Ignore the first field
	_ string `json:"-"`
	// Number of batteries sampled
	Count int `json:"count"`
	// Average voltage in mV
	Voltage int `json:"voltage"`
	// Total current in mA
	Current int `json:"current"`
	// Average capacity in %
	Capacity int `json:"capacity"`
}

// $BSTAT sentence
type Bstat struct {
	// Boot count
	Boot int `json:"boot"`
	// Uptime in seconds
	Uptime int `json:"uptime"`
}

// $STATS sentence
type Status struct {
	// Boot count
	Boot int `json:"boot"`
	// Uptime in seconds
	Uptime int `json:"uptime"`
	// Battery charge fault flag
	ChgFault uint `json:"chg_fault"`
	// Battery charge status flag
	ChgStatus uint `json:"chg_status"`
}

// Set a reflect.Value from the contents of the string src. Only integers
// (signed and unsigned), floats, and strings are supported. If the value
// is not changeable, this function will panic.
func setValue(v reflect.Value, src string, base int) error {
	switch v.Kind() {
	case reflect.Uint8:
		x, err := strconv.ParseUint(src, base, 8)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Uint16:
		x, err := strconv.ParseUint(src, base, 16)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Uint32:
		x, err := strconv.ParseUint(src, base, 32)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Uint64, reflect.Uint:
		x, err := strconv.ParseUint(src, base, 64)
		if err != nil {
			return err
		}
		v.SetUint(x)
	case reflect.Int8:
		x, err := strconv.ParseInt(src, base, 8)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int16:
		x, err := strconv.ParseInt(src, base, 16)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int32:
		x, err := strconv.ParseInt(src, base, 32)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Int64, reflect.Int:
		x, err := strconv.ParseInt(src, base, 64)
		if err != nil {
			return err
		}
		v.SetInt(x)
	case reflect.Float32, reflect.Float64:
		x, err := strconv.ParseFloat(src, 64)
		if err != nil {
			return err
		}
		v.SetFloat(x)
	case reflect.String:
		v.SetString(src)
	default:
		return fmt.Errorf("Unsupported type: %s", v.Kind())
	}
	return nil
}

// UnmarshalNMEA reads the contents of an NMEA sentence into the struct
// pointed to by v.
func UnmarshalNMEA(s nmea.Sentence, v interface{}) error {
	if um, ok := v.(NMEAUnmarshaler); ok {
		return um.UnmarshalNMEA(s)
	}

	val := reflect.ValueOf(v)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	if val.Kind() != reflect.Struct {
		return errors.New("Can only unmarshal into structs")
	}

	var (
		err  error
		base int
	)

	j := int(0)
	nf := len(s.Fields)

	for i := 0; i < val.NumField(); i++ {
		if fv := val.Field(i); fv.CanSet() {
			t := val.Type()
			// The presence of the "hex" tag means the value is
			// a hex encoded integer.
			if _, ok := t.Field(i).Tag.Lookup("hex"); ok {
				base = 16
			} else {
				base = 10
			}

			switch fv.Kind() {
			case reflect.Array:
				for k := 0; k < fv.Len(); k++ {
					if j >= nf {
						return fmt.Errorf("Invalid NMEA for %T: %q", v, s)
					}
					err = setValue(fv.Index(k), s.Fields[j], base)
					j++
				}
			default:
				if j >= nf {
					return fmt.Errorf("Invalid NMEA for %T: %q", v, s)
				}
				err = setValue(fv, s.Fields[j], base)
				j++
			}

			if err != nil {
				return fmt.Errorf("Cannot set field %d: %w", i, err)
			}
		} else {
			j++
		}
	}

	return nil
}

// RefmtSentence ensures that each distinct sentence type has a unique ID,
// namely the HCB sentences.
func RefmtSentence(s nmea.Sentence) nmea.Sentence {
	var sNew nmea.Sentence
	switch s.Id {
	case "HCB", "HCBRX":
		switch s.Fields[0] {
		case "TIME", "TEMP", "HEATPAD_PWM", "DESIRED_TEMP", "HCB_HYS", "MAX_PWM", "FAN1", "FAN2":
			s.Id = "HCB.SETTING"
			return s
		case "ALL_TEMP":
			sNew.Id = "HCB.ALLTEMP"
		default:
			sNew.Id = "HCB." + s.Fields[0]
		}
		sNew.Fields = s.Fields[1:]
	default:
		return s
	}

	return sNew
}

func DecodeSentence(s nmea.Sentence) (interface{}, error) {
	switch s.Id {
	case "ADC1R", "ADC0R", "ADCLR", "ADCRR", "ADC2R", "ADC3R", "ADC4R", "ADC5R":
		obj := RawAdc{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "HCB.ADC":
		obj := HcbAdc{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "IMAGR":
		obj := Imu{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "IMAGS":
		obj := Imu{}
		err := UnmarshalNMEA(s, &obj)
		return obj.Scale(), err
	case "IMMGR":
		obj := Mag{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "IMMGS":
		obj := Mag{}
		err := UnmarshalNMEA(s, &obj)
		return obj.Scale(), err
	case "BMEPR":
		obj := Env{}
		err := UnmarshalNMEA(s, &obj)
		return obj.Scale(), err
	case "POWER":
		obj := Power{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "FDBUG":
		obj := FocusDebug{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "FSTAT":
		obj := FocusStatus{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "FOCUS":
		obj := FocusCmd{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "HCB.ALLTEMP":
		obj := HcbTemp{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "HCB.SETTING", "HCB.SET":
		obj := HcbSetting{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "BAB":
		obj := Battery{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "STATS":
		obj := Status{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	case "BSTAT":
		obj := Bstat{}
		err := UnmarshalNMEA(s, &obj)
		return obj, err
	}
	return nil, fmt.Errorf("Invalid sentence type: %q", s.Id)
}
