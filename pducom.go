// Package pducom implements the NMEA-based communication protocol used by
// the inVADER PDU (Power Distribution board) and the PFU (Power Filter
// board).
package pducom

import (
	"bufio"
	"fmt"
	"io"
	"time"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

// Short names for the PDU sub-systems
type Subsystem string

const EOL = "\r"

const (
	TimingBoard    Subsystem = "TB"
	UvLights                 = "UV"
	BeamExpander             = "BEXP"
	Ccd                      = "CCD"
	HeaterCtlBoard           = "HCB"
	Camera1                  = "CAM1"
	Camera2                  = "CAM2"
	Led1                     = "LED1"
	Led2                     = "LED2"
	Expansion                = "EXPAN"
	FocusMotor               = "FOCUS"
)

// NMEA sentences to enable/disable the Laser
var (
	EnableLaser  = nmea.Sentence{Id: "LASER", Fields: []string{"1"}}
	DisableLaser = nmea.Sentence{Id: "LASER", Fields: []string{"0"}}
)

// IsValid validates a subsystem value.
func (s Subsystem) IsValid() bool {
	switch s {
	case TimingBoard, UvLights, BeamExpander, Ccd, HeaterCtlBoard:
	case Camera1, Camera2, Led1, Led2, Expansion, FocusMotor:
	default:
		return false
	}
	return true
}

// PwrState associates a sub-system with its power state
type PwrState struct {
	Sys     Subsystem `json:"sys"`
	Enabled bool      `json:"enabled"`
}

func (p PwrState) String() string {
	return fmt.Sprintf("%s,%v", p.Sys, p.Enabled)
}

// Device represents the communication interface to the PDU/PFU. It
// contains an input data scanner and an output writer.
type Device struct {
	rdr *bufio.Reader
	wtr io.Writer
	err error
	s   nmea.Sentence
	ts  time.Time
}

// Return a new PDU communication Device attached to an io.ReadWriter
func NewDevice(rw io.ReadWriter) *Device {
	return &Device{
		rdr: bufio.NewReader(rw),
		wtr: rw,
	}
}

// Scan advances the scanner to the next NMEA sentence which will be
// available through the Sentence method. It returns false when the scan
// stops, either by reaching the end of the input or an error.
func (d *Device) Scan() bool {
	d.s, d.err = nmea.ReadSentence(d.rdr)
	if d.err != nil {
		return false
	}
	d.ts = time.Now()
	return true
}

// Err returns the first non-EOF error that was encountered by the scanner
func (d *Device) Err() error {
	if d.err != io.EOF {
		return d.err
	}
	return nil
}

// Sentence returns the most recent nmea.Sentence generated by a call to
// Scan.
func (d *Device) Sentence() nmea.Sentence {
	return d.s
}

// Timestamp returns the read time of the most recent sentence.
func (d *Device) Timestamp() time.Time {
	return d.ts
}

// Pwren creates a PWREN NMEA sentence from a list of PwrState structs
func Pwren(states ...PwrState) nmea.Sentence {
	s := nmea.Sentence{Id: "PWREN", Fields: make([]string, 0, 2*len(states))}
	for _, state := range states {
		if state.Sys.IsValid() {
			s.Fields = append(s.Fields, string(state.Sys))
			if state.Enabled {
				s.Fields = append(s.Fields, "1")
			} else {
				s.Fields = append(s.Fields, "0")
			}
		}
	}
	return s
}

// Send writes an NMEA sentence to the device.
func (d *Device) Send(s nmea.Sentence) error {
	b, err := s.MarshalText()
	if err != nil {
		return err
	}
	d.wtr.Write(b)
	_, err = d.wtr.Write([]byte(EOL))
	return err
}
